from app import db 
from werkzeug.serving import make_ssl_devcert

db.create_all()
make_ssl_devcert('./ssl', host='localhost')